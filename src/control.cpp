#include "control.h"
#include "snake_game.h"
#include "menuitem.h"
#include "play.h"
#include "snake.h"
#include <hgeguictrls.h>
#include <math.h>

char * _opt_speed[3] = {
    "One",
    "Two",
	"Three"
};

const char * _instrtuction[] = {
    "Move to left - key left, right - key right,",
	"Move to bottom - key down, top - key up",
    "Exit from game to menu - space",
    NULL
};

float Snake::speed;

void Control_Menu::Render(Snake_Game *game)
{
	game->hge->Gfx_RenderQuad( &game->background_quad );
    gui->Render();
}

bool Control_Menu::Logic(Snake_Game * game)
{
	float dt = game->hge->Timer_GetDelta();
    int id;
    static int lastid = MENU_ELEMENT_NONE_SELECTED;

    if (game->hge->Input_GetKeyState(HGEK_ESCAPE)) {
        lastid = MENU_ELEMENT_EXIT;
        gui->Leave();
    }
   
	id = gui->Update(dt);
    if(id == -1)
    {
        switch(lastid)
        {
            case MENU_ELEMENT_PLAY:
                game->ShowPlayScreen();
                gui->Enter();
                break;
			case MENU_ELEMENT_INSTRUCTION:
				game->ShowInstructionScreen();
				gui->Enter();
				break;
			case MENU_ELEMENT_SPEED:
                game->ShowSpeedScreen();
                gui->Enter();
                break;
            case MENU_ELEMENT_EXIT:
                return true;
        }
    } else if(id) {
        lastid = id;
        gui->Leave();
    }
	return false;
}

Control_Menu::Control_Menu( Snake_Game * game )
{
    gui = new hgeGUI();
    gui->AddCtrl(new hgeGUIMenuItem(MENU_ELEMENT_PLAY, game->font, game->click_sound,
                        300, MENU_TOP_ITEM_Y, 0.0f, "Play"));
    gui->AddCtrl(new hgeGUIMenuItem(MENU_ELEMENT_SPEED, game->font, game->click_sound,
                        300, MENU_TOP_ITEM_Y+MENU_ITEM_HEIGHT, 0.1f, "Speed"));
	gui->AddCtrl(new hgeGUIMenuItem(MENU_ELEMENT_INSTRUCTION, game->font, game->click_sound,
                        300, MENU_TOP_ITEM_Y+2*MENU_ITEM_HEIGHT, 0.2f, "Instruction"));
    gui->AddCtrl(new hgeGUIMenuItem(MENU_ELEMENT_EXIT, game->font, game->click_sound,
                        300, MENU_TOP_ITEM_Y+3*MENU_ITEM_HEIGHT, 0.3f, "Exit"));

    gui->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
    gui->SetCursor( game->mouse_cursor_sprite );
    gui->SetFocus(MENU_ELEMENT_PLAY);
    gui->Enter();
}

Control_Menu::~Control_Menu()
{
    delete gui;
}


Control_Instruction::Control_Instruction( Snake_Game * game )
{
	gui = new hgeGUI();
	PopulateInstructionsPage( game, _instrtuction);
	gui->AddCtrl(new hgeGUIMenuItem(MENU_ELEMENT_EXIT, game->font, game->click_sound,
		300, MENU_TOP_ITEM_Y+2*MENU_ITEM_HEIGHT, 0.1f, "Exit"));

    gui->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
    gui->SetCursor( game->mouse_cursor_sprite );
	gui->SetFocus( MENU_ELEMENT_INSTRUCTION );
    gui->Enter();
}

void Control_Instruction::PopulateInstructionsPage( Snake_Game * game, const char * text[] )
{
    for( int textrow = 0; text[textrow]; textrow++ )
    {
    
		if (gui->GetCtrl( 100+textrow )) {
            gui->DelCtrl( 100+textrow );
        }

		hgeGUIText * c1 = new hgeGUIText(100+textrow, 0, 150.0f+30.0f*textrow, 600, 30, game->font);
        c1->SetMode( HGETEXT_CENTER );
        c1->SetText( text[textrow] );
        gui->AddCtrl(c1);
    }
}

Control_Instruction::~Control_Instruction()
{
    delete gui;
}


void Control_Instruction::Render(Snake_Game * game )
{
    game->hge->Gfx_RenderQuad( &game->background_quad );
    gui->Render();
}


bool Control_Instruction::Logic(Snake_Game * game )
{
    float dt = game->hge->Timer_GetDelta();
    int id;
	static int lastid = MENU_ELEMENT_NONE_SELECTED;

	if (game->hge->Input_GetKeyState(HGEK_ESCAPE)) {
		lastid = MENU_ELEMENT_EXIT;
        gui->Leave();
    }
    
    id = gui->Update(dt);
    if(id == -1)
    {
        switch(lastid)
        {
		case MENU_ELEMENT_INSTRUCTION:
			 PopulateInstructionsPage( game, _instrtuction);
             gui->SetFocus(MENU_ELEMENT_INSTRUCTION);
             gui->Enter();
             break;
		case MENU_ELEMENT_EXIT:
            game->ShowMenuScreen();
            gui->Enter();
            break;
        }
    } else if(id) {
        lastid = id;
        gui->Leave();
    }

    return false;
}


Control_Speed::Control_Speed( Snake_Game * game )
{
	Snake::speed = 0.5f;
    gui = new hgeGUI();
	gui->AddCtrl(new hgeGUIMenuItem(MENU_ELEMENT_SPEED, game->font, game->click_sound,
		300, MENU_TOP_ITEM_Y-MENU_ITEM_HEIGHT, 0.0f, _opt_speed[game->opt_speed]));
	gui->AddCtrl(new hgeGUIMenuItem(MENU_ELEMENT_EXIT, game->font, game->click_sound,
		300, MENU_TOP_ITEM_Y, 0.1f, "Exit"));

    gui->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
    gui->SetCursor( game->mouse_cursor_sprite );
	gui->SetFocus( MENU_ELEMENT_SPEED );
    gui->Enter();
}


Control_Speed::~Control_Speed()
{
    delete gui;
}


void Control_Speed::Render(Snake_Game * game )
{
    game->hge->Gfx_RenderQuad( &game->background_quad );
    gui->Render();
}

bool Control_Speed::Logic(Snake_Game * game )
{
    float dt = game->hge->Timer_GetDelta();
    int id;
	static int lastid = MENU_ELEMENT_NONE_SELECTED;
    if (game->hge->Input_GetKeyState(HGEK_ESCAPE)) {
		lastid = MENU_ELEMENT_EXIT;
        gui->Leave();
    }
    
    id = gui->Update(dt);
    if(id == -1)
    {
        switch(lastid)
        {
		case MENU_ELEMENT_SPEED:
			switch(game->opt_speed)
			{
			case 0:
				game->opt_speed++;
				Snake::speed = 0.3f;
				break;
			case 1:
					game->opt_speed++;
					Snake::speed = 0.2f;
					break;
			case 2:
					game->opt_speed = 0;
					Snake::speed = 0.1f;
					break;
			}
            
			gui->DelCtrl(MENU_ELEMENT_SPEED);
			gui->AddCtrl(new hgeGUIMenuItem(MENU_ELEMENT_SPEED, game->font, game->click_sound,
				300, MENU_TOP_ITEM_Y-MENU_ITEM_HEIGHT, 0.0f, _opt_speed[game->opt_speed] ));

			gui->SetFocus( MENU_ELEMENT_SPEED );
            gui->Enter();
            break;

		case MENU_ELEMENT_EXIT:
            game->ShowMenuScreen();
            gui->Enter();
            break;
        }
    } else if(id) {
        lastid = id;
        gui->Leave();
    }

    return false;
}

void Control_Play::Render( Snake_Game * game )
{
	if(play) play->Render();
}


bool Control_Play::Logic( Snake_Game * game )
{
	if(play->Logic())
	{
		game->ShowMenuScreen();
	}
	if(play->Win())
	{
		game->ShowMenuScreen();
	}
	return false;
}


void Control_Play::EnterState( Snake_Game * game )
{
    play = new Play();
}

Control_Play::Control_Play()
	:play(NULL)
{

}


Control_Play::~Control_Play()
{
    delete play;
}

