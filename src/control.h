#include <hgegui.h>

class Snake_Game;
class Snake;
class Control 
{
public:
	virtual ~Control() {}
	virtual void Render(Snake_Game * game) = 0;
	virtual bool Logic(Snake_Game * game) = 0;
	virtual void EnterState(Snake_Game * game) {}
};

class Control_Menu: public virtual Control
{
protected: 
	hgeGUI * gui;
public:
	
	Control_Menu(Snake_Game * game);
	virtual ~Control_Menu();
	void Render(Snake_Game * game);
	bool Logic(Snake_Game * game);

	static const int MENU_TOP_ITEM_Y = 100;
    static const int MENU_ITEM_HEIGHT =40;

	enum {
        MENU_ELEMENT_NONE_SELECTED = 0,
        MENU_ELEMENT_PLAY = 1,
        MENU_ELEMENT_SPEED,
		MENU_ELEMENT_INSTRUCTION,
        MENU_ELEMENT_EXIT
    };

};

class Control_Speed: public virtual Control
{
protected: 
	hgeGUI * gui;
public:
	Control_Speed(Snake_Game * game);
	virtual ~Control_Speed();
	void Render(Snake_Game * game);
	bool Logic(Snake_Game * game);

	static const int MENU_TOP_ITEM_Y = 200;
    static const int MENU_ITEM_HEIGHT = 40;

	enum {
        MENU_ELEMENT_NONE_SELECTED = 0,
        MENU_ELEMENT_SPEED = 1,
        MENU_ELEMENT_EXIT
    };

};

class Control_Instruction: public virtual Control
{
protected: 
	hgeGUI * gui;
public:
	Control_Instruction(Snake_Game * game);
	virtual ~Control_Instruction();
	void Render(Snake_Game * game);
	bool Logic(Snake_Game * game);
	void PopulateInstructionsPage( Snake_Game * game, const char * text[] );
	static const int MENU_TOP_ITEM_Y = 200;
    static const int MENU_ITEM_HEIGHT = 40;

	enum {
        MENU_ELEMENT_NONE_SELECTED = 0,
        MENU_ELEMENT_INSTRUCTION = 1,
        MENU_ELEMENT_EXIT
    };

};

class Play;

class Control_Play: public virtual Control
{
protected: 
	Play * play;
public:
	Control_Play();
	virtual ~Control_Play();
	void Render(Snake_Game * game);
	bool Logic(Snake_Game * game);
	
	void EnterState(Snake_Game * game);
};