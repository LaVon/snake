#include "food.h"
#include "snake.h"

Food::Food()
{
	appetite = 15;
}

void Food::CreateEat(int width, int height, int dx, int dy)
{
	srand((unsigned)time(0)); 
	eat.push_back(make_pair(rand()%(width/dx)*dx, rand()%(height/dy)*dy));
}

void Food::DeleteEat()
{
	eat.pop_back();
}
