#include <vector>
#include <ctime> 
using namespace std;

class Food
{
public:
	vector<pair<float,float> > eat;
	int appetite;
	Food();
	void CreateEat(int width, int height, int dx, int dy);
	void DeleteEat();
};