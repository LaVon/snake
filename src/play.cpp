#include "play.h"
#include "food.h"
#include "snake.h"

Play::Play()
{
	food = new Food();
	snake = new Snake();
	hge = hgeCreate( HGE_VERSION );
	food->CreateEat(int(snake->screenWidth), int(snake->screenHeight), int (snake->snakeDX), int(snake->snakeDY));

	snaketxt = hge->Texture_Load("bg.png");
	eattxt = hge->Texture_Load("bg.png");	
	if(!snaketxt || !eattxt)
		{
			Message(2);
			hge->System_Shutdown();
			hge->Release();
		}	
	snakespt = new hgeSprite(snaketxt, 8,8,8,8);
	eatspt = new hgeSprite(eattxt, 8,8,8,8);
	snakespt->SetColor(0xFFFFA000);

}

Play::~Play()
{
	delete snakespt;
	delete eatspt;
	hge->Texture_Free(snaketxt);
	hge->Texture_Free(eattxt);
}

void Play::Message(int type)
{
	switch (type)
	{
	case 1: MessageBoxA(NULL, "You are lose", "Oopps", MB_OK | MB_ICONINFORMATION | MB_APPLMODAL);
		break;
	case 2: MessageBoxA(NULL, "No background", "Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
		break;
	case 3: MessageBoxA(NULL, "Congratulations! You are win!!!", "Win", MB_OK | MB_ICONINFORMATION | MB_APPLMODAL);
		break;
	}	
}

bool Play::Collision()
{
	snake->head = snake->snake.begin();
	if((*snake->head).first > snake->screenWidth - snake->snakeDX || (*snake->head).first < 0 || (*snake->head).second > snake->screenHeight - snake->snakeDY || (*snake->head).second < 0)
				{
					Message(1);				
					return true;
				}	
	for (snake->snakeIter = std::next(snake->snake.begin(), 1); snake->snakeIter != snake->snake.end(); ++snake->snakeIter)
	{
		if((*snake->snakeIter).first == (*snake->head).first && (*snake->snakeIter).second == (*snake->head).second)
		{
			Message(1);
			return true;
		}
	}
	if((*snake->head).first == food->eat[0].first && (*snake->head).second == food->eat[0].second)
		{
			snake->snake.push_back(make_pair(food->eat[0].first, food->eat[0].second));
			food->DeleteEat();
			snake->speed-=snake->deltaSpeed;
			food->CreateEat(int(snake->screenWidth), int(snake->screenHeight), int (snake->snakeDX), int(snake->snakeDY));
		}
	return false;
}

bool Play::Win()
{
	if(snake->snake.size() == food->appetite)
	{	
		Message(3);
		return true;
	}
	return false;
}

bool Play::Logic()
{
	float dt = hge->Timer_GetDelta();
	if (hge->Input_GetKeyState(HGEK_SPACE)) return true;
	if(snake->frameCount < snake->speed)
		snake->frameCount+=dt;
	else
	{
		snake->frameCount = 0;
		if(Collision() || Win()) return true;
		snake->Update(snake->direction);

	}
	switch(snake->direction)
		{
		case 1: 
		case 3:
			if (hge->Input_GetKeyState(HGEK_UP)) 
		{	
			snake->Turn(4);		
		}
			if (hge->Input_GetKeyState(HGEK_DOWN)) 
		{		
			snake->Turn(2);
		}
				break;
		case 2:
		case 4:
			if (hge->Input_GetKeyState(HGEK_LEFT)) 
		{		
			snake->Turn(3);
		}
			if (hge->Input_GetKeyState(HGEK_RIGHT)) 
		{	
			snake->Turn(1);
		}
				break;
	}
	// 1 - right, 2 - down, 3 - left, 4 - up
	
	return false;
}

bool Play::Render()
{
	hge->Gfx_BeginScene(target);
	
	hge->Gfx_Clear(0);
	for (snake->snakeIter = snake->snake.begin(); snake->snakeIter != snake->snake.end(); ++snake->snakeIter)
	{
		snakespt->Render((*snake->snakeIter).first, (*snake->snakeIter).second);		
	}
		eatspt->Render(food->eat[0].first, food->eat[0].second);

	hge->Gfx_EndScene();

	return false;
}