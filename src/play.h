#include <hge.h>
#include <hgerect.h>
#include <hgevector.h>
#include <hgesprite.h>

class Snake;
class Food;

class Play
{
public:
	HGE *hge;
	hgeSprite* snakespt;
	hgeSprite* eatspt;

	HTEXTURE snaketxt;
	HTEXTURE eattxt;
	HTARGET target;

	Food  *food;
	Snake *snake;

public:
	Play();
	void Message(int type);
	bool Collision();
	bool Win();
	bool Logic();
	bool Render();
	~Play();
};