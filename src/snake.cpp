#include "snake.h"
#include "food.h"

Snake::Snake()
{
	screenWidth = 603.0f;
	screenHeight = 405.0f;
	snakeDX = 9.0f; snakeDY = 9.0f;
	float x = (int)(screenWidth/snakeDX)/2 * snakeDX;
	float y = (int)(screenHeight/snakeDY)/2 * snakeDY;
	direction = 1;
	frameCount = 0.0f;
	deltaSpeed = 0.04f;
	
	
		for(int i = 0; i < 3; i++)
		{
			snake.push_front(make_pair(x, y));
			x += (int)snakeDX;
		}
		head = snake.begin();
}

void Snake::Turn(int direct)
{
	frameCount = 0;
	direction = direct;	
}

void Snake::Update(int direct)
{
	  float tail [2][2];
	  tail[0][0] = (*head).first;
      tail[0][1] = (*head).second;
		switch (direct)
		{
		case 1: 
			(*head).first+=snakeDX;
			break;
		case 3:
			(*head).first-=snakeDX;
			break;
		case 4: 
			(*head).second-=snakeDY;
			break;
		case 2:
			(*head).second+=snakeDY;
			 break;
		}
			tail[1][0] = (*head).first;
			tail[1][1] = (*head).second;

			snake.pop_back();
			snake.pop_front();
			snake.push_front(make_pair(tail[0][0], tail[0][1]));
			snake.push_front(make_pair(tail[1][0], tail[1][1]));
}
