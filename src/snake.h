#include <list>

using namespace std;

class Snake
{
public:
	list<pair<float,float> > snake;
	list <pair <float, float> >::iterator head, snakeIter;
	int direction;
	float frameCount,deltaSpeed;
	static float  speed;
	float snakeDX, snakeDY;
	float screenWidth, screenHeight;
	Snake();
	void Update(int direct);
	void Turn(int direct);
};