#include "snake_game.h"
#include "control.h"
#include <stdio.h>
using namespace std;

Snake_Game * Snake_Game::game = NULL;

Snake_Game::Snake_Game()
	: hge(NULL), font(NULL), mouse_cursor_sprite(NULL), control(NULL), opt_speed(0)
{
	game = this;
}

bool Snake_Game::FrameFunc()
{
	return game->control->Logic(game);
}

bool Snake_Game::RenderFunc()
{
	game->hge->Gfx_BeginScene();

	game->control->Render(game);

	char fps_text[64]; 
    _snprintf( fps_text, sizeof(fps_text)-1,
                "dt:%.3f\nFPS:%d",
                game->hge->Timer_GetDelta(), game->hge->Timer_GetFPS()
                );

    game->font->SetColor(ARGB(255,0,0,0)); 
    game->font->printf( 7, 7, HGETEXT_LEFT, fps_text );
    game->font->SetColor(ARGB(255,255,255,255)); 
    game->font->printf( 5, 5, HGETEXT_LEFT, fps_text );
    
    game->hge->Gfx_EndScene();

    return false;
}

bool Snake_Game::Startup()
{
    hge = hgeCreate(HGE_VERSION);

    hge->System_SetState(HGE_SHOWSPLASH, false);
    hge->System_SetState(HGE_FPS, 60);

    hge->System_SetState(HGE_LOGFILE, "hgelog.log");
    hge->System_SetState(HGE_FRAMEFUNC, FrameFunc);
    hge->System_SetState(HGE_RENDERFUNC, RenderFunc);
    hge->System_SetState(HGE_TITLE, "Control the snake");
    hge->System_SetState(HGE_WINDOWED, true);
    hge->System_SetState(HGE_SCREENWIDTH, 603);
    hge->System_SetState(HGE_SCREENHEIGHT, 405);
    hge->System_SetState(HGE_SCREENBPP, 32);

    if( !hge->System_Initiate() ) return false;

	background_quad.tex = hge->Texture_Load("bg.png");
    mouse_cursor_tex = hge->Texture_Load("cursor.png");
    click_sound = hge->Effect_Load("menu.wav");

    if(!background_quad.tex || !mouse_cursor_tex || !click_sound)
    {
        MessageBoxA(NULL, "Can't load BG.PNG, CURSOR.PNG or MENU.WAV", "Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
        hge->System_Shutdown();
        hge->Release();
        return false;
    }

    background_quad.blend=BLEND_ALPHABLEND | BLEND_COLORMUL | BLEND_NOZWRITE;

    for(int i=0;i<4;i++)
    {
        background_quad.v[i].z=0.5f;
        background_quad.v[i].col=ARGB(0xFF, 0xFF, 0xFF, 0xFF);
    }

    background_quad.v[0].x=0;
    background_quad.v[0].y=0; 

    background_quad.v[1].x=800;
    background_quad.v[1].y=0;

    background_quad.v[2].x=800;
    background_quad.v[2].y=600; 

    background_quad.v[3].x=0;
    background_quad.v[3].y=600; 

    font = new hgeFont("font1.fnt");
    mouse_cursor_sprite = new hgeSprite(mouse_cursor_tex,0,0,32,32);

	control_menu = new Control_Menu(this);
    control_speed = new Control_Speed( this );
    control_play = new Control_Play();
	control_instruction = new Control_Instruction(this);
	ShowMenuScreen();   

    return true;
}

void Snake_Game::Shutdown()
{
	delete control_menu;
    delete control_speed;
    delete control_play;

    delete font;
    delete mouse_cursor_sprite;

    hge->Effect_Free(click_sound);
    hge->Texture_Free(mouse_cursor_tex);
    hge->Texture_Free(background_quad.tex);

    hge->System_Shutdown();
    hge->Release();
}


void Snake_Game::ShowMenuScreen()
{
    control = control_menu;
}


void Snake_Game::ShowSpeedScreen()
{
	control = control_speed;
}

void Snake_Game::ShowInstructionScreen()
{
	control = control_instruction;
}

void Snake_Game::ShowPlayScreen()
{
	control = control_play;
	control_play->EnterState( this );
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Snake_Game game;
    if( game.Startup() )
    {
        game.hge->System_Start(); 
        game.Shutdown();
    }
}
