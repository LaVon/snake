#include <stdint.h>
#include <hge.h>
#include <hgefont.h>

class Control;

class Snake_Game 
{
public:
	static Snake_Game * game;
	
	HGE * hge;
	
	HEFFECT  click_sound;
	HTEXTURE    mouse_cursor_tex;
	
    hgeSprite * mouse_cursor_sprite;

	hgeFont* font;

	int opt_speed;

	hgeQuad  background_quad;

	Control * control;

	Control * control_menu;
	Control * control_speed;
	Control * control_play;
	Control * control_instruction;

public:
	Snake_Game();
	
	static bool FrameFunc();
	static bool RenderFunc();

	bool Startup();
	void Shutdown();

	void ShowMenuScreen();
	void ShowSpeedScreen();
	void ShowPlayScreen();
	void ShowInstructionScreen();

};